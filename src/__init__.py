from collections import defaultdict
from dataclasses import dataclass


@dataclass
class ItemConfig:
    sku: str
    price: int

    # indicates whether times_scanned and reduce_by shall be processed
    has_multiprice: bool

    # prevent divsion by 0 by defaulting to 1
    times_scanned: int = 1
    reduce_by: int = 0


class CheckoutConfig:
    def __init__(self):
        self._item_configs = {}

    def add_item_config(self, item_config: ItemConfig):
        self._item_configs[item_config.sku] = item_config

    def get_item_price(self, sku: str, times_scanned: int) -> int:
        item_config: ItemConfig = self._item_configs[sku]

        if (item_config.has_multiprice and
                times_scanned % item_config.times_scanned == 0):
            return item_config.price - item_config.reduce_by

        return item_config.price


class Checkout:
    def __init__(self, config: CheckoutConfig):
        self._total = 0
        self._config = config
        # keeps track what SKU was scanned how often
        self._sku_count = defaultdict(int)

    def scan(self, sku: str):
        self._sku_count[sku] += 1
        times_scanned = self._sku_count[sku]
        price = self._config.get_item_price(sku, times_scanned)
        self._total += price

    @property
    def total(self) -> int:
        # make total read only
        return self._total
