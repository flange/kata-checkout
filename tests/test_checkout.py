import pytest

from src import Checkout, CheckoutConfig, ItemConfig


@pytest.fixture(scope='module')
def checkout_config():
    checkout_config = CheckoutConfig()
    checkout_config.add_item_config(ItemConfig('A', 50, True, 3, 20))
    checkout_config.add_item_config(ItemConfig('B', 30, True, 2, 15))
    checkout_config.add_item_config(ItemConfig('C', 20, False))
    checkout_config.add_item_config(ItemConfig('D', 15, False))
    return checkout_config


def test_incremental_simple(checkout_config):
    checkout = Checkout(checkout_config)
    assert checkout.total == 0
    checkout.scan('A')
    assert checkout.total == 50
    checkout.scan('B')
    assert checkout.total == 80
    checkout.scan('C')
    assert checkout.total == 100
    checkout.scan('D')
    assert checkout.total == 115


def test_total_null(checkout_config):
    checkout = Checkout(checkout_config)
    assert checkout.total == 0


def test_checkout_multi_price(checkout_config):
    checkout = Checkout(checkout_config)
    checkout.scan('A')
    checkout.scan('A')

    assert checkout.total == 100

    checkout.scan('A')

    assert checkout.total == 130


def test_checkout_non_multi_price(checkout_config):
    checkout = Checkout(checkout_config)
    checkout.scan('D')
    checkout.scan('D')
    checkout.scan('D')

    assert checkout.total == 45


def test_checkout_multi_price_twice(checkout_config):
    checkout = Checkout(checkout_config)
    checkout.scan('B')
    checkout.scan('B')
    checkout.scan('B')
    checkout.scan('B')

    assert checkout.total == 90
