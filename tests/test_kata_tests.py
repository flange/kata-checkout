import pytest

from src import Checkout, CheckoutConfig, ItemConfig


@pytest.fixture(scope='module')
def checkout_config():
    checkout_config = CheckoutConfig()
    checkout_config.add_item_config(ItemConfig('A', 50, True, 3, 20))
    checkout_config.add_item_config(ItemConfig('B', 30, True, 2, 15))
    checkout_config.add_item_config(ItemConfig('C', 20, False))
    checkout_config.add_item_config(ItemConfig('D', 15, False))
    return checkout_config


def price(goods, checkout_config):
    checkout = Checkout(checkout_config)
    [checkout.scan(item) for item in goods]
    return checkout.total


def test_incremental(checkout_config):
    co = Checkout(checkout_config)

    co.scan('A')
    assert 50 == co.total
    co.scan('B')
    assert 80 == co.total
    co.scan('A')
    assert 130 == co.total
    co.scan('A')
    assert 160 == co.total
    co.scan('B')
    assert 175 == co.total


def test_totals(checkout_config):
    # pytest gives warnings when we call checkout_config directly
    # so we have to use it as a fixture and then pass it to price()
    assert 0 == price('', checkout_config)
    assert 50 == price('A', checkout_config)
    assert 80 == price('AB', checkout_config)
    assert 115 == price('CDBA', checkout_config)

    assert 100 == price('AA', checkout_config)
    assert 130 == price('AAA', checkout_config)
    assert 180 == price('AAAA', checkout_config)
    assert 230 == price('AAAAA', checkout_config)
    assert 260 == price('AAAAAA', checkout_config)

    assert 160 == price('AAAB', checkout_config)
    assert 175 == price('AAABB', checkout_config)
    assert 190 == price('AAABBD', checkout_config)
    assert 190 == price('DABABA', checkout_config)
